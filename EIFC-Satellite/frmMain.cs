﻿using CW;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using static EIFC_Satellite.SatelliteErrors;
using Color = System.Drawing.Color;

namespace EIFC_Satellite
{

    public partial class frmMain : Form
    {
        public SqlConnection sqlConn, sqlCTCConn;
        public int totfilesRx;
        public int totfilesTx;
        public int cwRx;
        public int cwTx;
        public int filesRx;
        public int filesTx;
        public System.Timers.Timer tmrMain;

        public frmMain()
        {
            InitializeComponent();
            tmrMain = new System.Timers.Timer();
            Assembly ass = Assembly.GetExecutingAssembly();
            AssemblyName assName = ass.GetName();
            Version ver = assName.Version;
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
            this.Text = string.Format("{0}, Version", assName.Name, ver.ToString());

        }

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop")
            {
                tmrMain.Stop();
                GetFiles(Globals.glPickupPath);
                if (!string.IsNullOrEmpty(Globals.glCustomFilesPath))
                {
                    GetFiles(Globals.glCustomFilesPath);
                }

                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
            }


        }

        private void OnProcessExit(object sender, EventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Stopping", m.GetParameters());
        }

        private DataTable ExcelToTable(string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {

                SpreadsheetDocument xs = SpreadsheetDocument.Open(fs, false);
                WorkbookPart workbookPart = xs.WorkbookPart;
                IEnumerable<Sheet> sheets = xs.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)xs.WorkbookPart.GetPartById(relationshipId);
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                DataTable dt = new DataTable();
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Reading XLS Data");
                foreach (Cell cell in rows.ElementAt(0))
                {
                    DataColumnCollection colColl = dt.Columns;
                    string colName = ExcelHelper.GetCellValue(xs, cell).Trim();
                    int i = 1;
                    while (colColl.Contains(colName))
                    {
                        colName = ExcelHelper.GetCellValue(xs, cell).Trim() + "-" + i;
                        i++;
                    }
                    dt.Columns.Add(colName);
                }

                foreach (Row row in rows)
                {
                    try
                    {
                        DataRow tempRow = dt.NewRow();
                        int columnIndex = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            int cellColumnIndex = (int)ExcelHelper.GetColumnIndexFromName(ExcelHelper.GetColumnName(cell.CellReference));
                            cellColumnIndex--;
                            if (columnIndex < cellColumnIndex)
                            {
                                do
                                {
                                    tempRow[columnIndex] = "";
                                    columnIndex++;
                                }
                                while (columnIndex < cellColumnIndex);

                            }
                            tempRow[columnIndex] = ExcelHelper.GetCellValue(xs, cell).Trim();
                            columnIndex++;
                            if (dt.Rows.Count == 840)
                            {
                            }
                        }

                        dt.Rows.Add(tempRow);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.GetType().Name + " Error found. Rows Processed: " + dt.Rows.Count + "Error: " + ex.Message);
                    }
                }


                fs.Close();
                Globals.ArchiveFile(Globals.glArcLocation, fileName);
                try
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    //        File.Delete(xmlFile);
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Exception:" + strEx + ". Unable to Delete " + Path.GetFileName(fileName) + ":" + ex.Message, System.Drawing.Color.Red);

                }
                dt.Rows.RemoveAt(0);
                return dt;
                DataSet ds = new DataSet();

            }
        }

        private String getCustPath(String senderID)
        {
            SqlCommand sqlCustPath = new SqlCommand("SELECT @C_PATH=C_PATH from CUSTOMER where C_CODE = @C_CODE", sqlConn);
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlCustPath.Parameters.AddWithValue("@C_CODE", senderID);
            SqlParameter CUSTPATH = sqlCustPath.Parameters.Add("@C_PATH", SqlDbType.VarChar, -1);
            CUSTPATH.Direction = ParameterDirection.Output;
            sqlCustPath.CommandType = CommandType.Text;
            sqlCustPath.ExecuteNonQuery();
            if (CUSTPATH.Value != DBNull.Value)
            {
                return CUSTPATH.Value.ToString();
            }
            else
            {
                return String.Empty;
            }
        }

        private Guid getCustID(String senderID)
        {
            SqlCommand sqlCustID = new SqlCommand("SELECT @C_ID=C_ID from CUSTOMER where C_CODE = @C_CODE", sqlConn);
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlCustID.Parameters.AddWithValue("@C_CODE", senderID);
            SqlParameter CUSTID = sqlCustID.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
            CUSTID.Direction = ParameterDirection.Output;
            sqlCustID.CommandType = CommandType.Text;
            sqlCustID.ExecuteNonQuery();
            if (CUSTID.Value != DBNull.Value)
            {
                return (Guid)CUSTID.Value;
            }
            else
            {
                return Guid.Empty;
            }
        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = System.Drawing.Color.LightCoral;
                UpdateSBar("Timed Processes Started");
                GetFiles(Globals.glPickupPath);
                if (!string.IsNullOrEmpty(Globals.glCustomFilesPath))
                {
                    GetFiles(Globals.glCustomFilesPath);
                }
                //  PrintJobs("");
                tmrMain.Start();

            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = System.Drawing.Color.LightGreen;
                UpdateSBar("Timed Processes Stopped");
                tmrMain.Stop();
            }
        }

        private void UpdateSBar(string text)
        {
            tslMain.Text = text;
            statusStrip1.Invalidate();
            statusStrip1.Refresh();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();
            LoadSettings();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            LoadSettings();
            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Starting", null);
        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);


            }
            tmrMain.Interval = 60 * (1000 * 5);

            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.glAppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.glAppConfig = Path.Combine(path, Globals.glAppConfig);
            frmSettings Settings = new frmSettings();
            if (!File.Exists(Globals.glAppConfig))
            {
                XDocument xmlConfig = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("Satellite",
                    new XElement("Customer"),
                    new XElement("Database"),
                    new XElement("CTCNode"),
                    new XElement("Communications")));
                xmlConfig.Save(Globals.glAppConfig);
                Settings.ShowDialog();
            }
            else
            {
                try
                {
                    bool loadConfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.glAppConfig);
                    // Configuration Section DataBase
                    XmlNodeList nodeList = xmlConfig.SelectNodes("/Satellite/Database");
                    XmlNode nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    sqlConn = new SqlConnection();
                    sqlConn.ConnectionString = Globals.connString();

                    nodeList = null;
                    //Configuration Section Customer
                    nodeList = xmlConfig.SelectNodes("/Satellite/Customer");

                    XmlNode xCustCode = nodeList[0].SelectSingleNode("Code");
                    if (xCustCode != null)
                    {
                        Globals.glCustCode = xCustCode.InnerText;
                        Globals.Customer cust = new Globals.Customer();
                        cust = Globals.GetCustomer(Globals.glCustCode);
                        if (cust != null)
                        {
                            Globals.glCustCode = cust.Code;
                            Globals.gl_CustId = cust.Id;
                            Globals.gl_Customer = cust.CustomerName;
                        }
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPath = nodeList[0].SelectSingleNode("ProfilePath");
                    if (xPath != null)
                    {
                        Globals.glProfilePath = xPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xTestPath = nodeList[0].SelectSingleNode("TestPath");
                    if (xTestPath != null)
                    {
                        Globals.glTestLocation = xTestPath.InnerText;
                    }
                    XmlNode xArchivePath = nodeList[0].SelectSingleNode("ArchiveLocation");
                    if (xArchivePath != null)
                    {
                        Globals.glArcLocation = xArchivePath.InnerText;
                    }
                    XmlNode xCustomPath = nodeList[0].SelectSingleNode("CustomFilesPath");
                    if (xCustomPath != null)
                    {
                        Globals.glCustomFilesPath = xCustomPath.InnerText;
                    }

                    XmlNode xOutPath = nodeList[0].SelectSingleNode("OutputPath");
                    if (xOutPath != null)
                    {
                        Globals.glOutputDir = xOutPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xInPath = nodeList[0].SelectSingleNode("PickupPath");
                    if (xInPath != null)
                    {
                        Globals.glPickupPath = xInPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xLib = nodeList[0].SelectSingleNode("LibraryPath");
                    if (xLib != null)
                    {
                        Globals.glLibPath = xLib.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xFail = nodeList[0].SelectSingleNode("FailPath");
                    if (xFail != null)
                    {
                        Globals.glFailPath = xFail.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    if (loadConfig)
                    {
                        throw new System.Exception("Mandatory Settings missing");
                    }

                    // Configuration Section Communications
                    nodeList = xmlConfig.SelectNodes("/Satellite/Communications");

                    XmlNode xmlAlertsTo = nodeList[0].SelectSingleNode("AlertsTo");
                    if (xmlAlertsTo != null)
                    {
                        Globals.glAlertsTo = xmlAlertsTo.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xSmtp = nodeList[0].SelectSingleNode("SMTPServer");
                    if (xSmtp != null)
                    {
                        SMTPServer.Server = xSmtp.InnerText;
                        SMTPServer.Port = xSmtp.Attributes[0].InnerXml;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xEmail = nodeList[0].SelectSingleNode("EmailAddress");
                    if (xEmail != null)
                    { SMTPServer.Email = xEmail.InnerText; }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xUserName = nodeList[0].SelectSingleNode("SMTPUsername");
                    if (xUserName != null)
                    {
                        SMTPServer.Username = xUserName.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPassword = nodeList[0].SelectSingleNode("SMTPPassword");
                    if (xPassword != null)
                    {
                        SMTPServer.Password = xPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }
                    //CTC Node Database Connection
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/CTCNode");
                    nodeCat = null;
                    nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glCTCDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlServer = null;
                    xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glCTCDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlUser = null;
                    xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlPassword = null;
                    xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    sqlCTCConn = new SqlConnection();
                    sqlCTCConn.ConnectionString = Globals.CTCconnString();
                }

                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }
                var Cust = Globals.GetCustomer(Globals.glCustCode);
                try
                {
                    this.Text = "CTC Satellite - " + Cust.CustomerName;
                }
                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }

                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "System Ready");
            }
        }



        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfiles Profiles = new frmProfiles();
            Profiles.ShowDialog();

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetFiles("Processing");
            GetFiles("Customs");

        }

        private void GetFiles(string folder)
        {
            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                filesPath = Globals.glPickupPath;
            }
            else
            {
                filesPath = Globals.glTestLocation;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();
            int iCount = 0;
            var stopwatch = new Stopwatch();

            var list = diTodo.GetFiles();
            if (list.Length > 0)
            {
                stopwatch.Start();
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing " + list.Length + " Files");
                foreach (var fileName in diTodo.GetFiles())
                {
                    iCount++;
                    ProcessResult thisResult = new ProcessResult();
                    thisResult = ProcessCustomFiles(fileName.FullName);
                    //return;
                    if (thisResult.Processed)
                    {
                        try
                        {
                            if (File.Exists(fileName.FullName))
                            {
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();
                                File.Delete(fileName.FullName);
                            }


                        }
                        catch (Exception ex)
                        {
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to Delete File. Error :" + ex.Message);
                        }

                    }

                }
                stopwatch.Stop();
                double t = stopwatch.Elapsed.TotalSeconds;
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Complete. " + iCount + " files processed in " + t + " seconds");
            }
            else
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Waiting for files.");
            }
        }

        private Guid GetCSVFileType(string fileName)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            Guid result = Guid.Empty;
            SqlDataAdapter fileProfile = new SqlDataAdapter("SELECT [PC_ID],[PC_P],[PC_FileName],[PC_HasHeader],[PC_Delimiter],[PC_Fieldcount],[PC_FirstFieldName],[PC_LastFieldName],[PC_Quotations]" +
                                                        ",[PC_HeaderStart],[PC_DataStart]" +
                                                        "FROM[PCFS].[dbo].[FileDescription]", sqlConn);
            DataSet dsFileProfile = new DataSet();
            fileProfile.Fill(dsFileProfile, "FileProfile");
            foreach (DataRow dr in dsFileProfile.Tables[0].Rows)
            {
                try
                {
                    string headerText = string.Empty;
                    StreamReader sr = new StreamReader(fileName);
                    int headerstart = 0;

                    if (int.TryParse(dr["PC_HEADERSTART"].ToString(), out headerstart))
                    {
                        if (headerstart > 0)
                        {
                            for (int i = 0; i <= headerstart; i++)
                            {
                                sr.ReadLine();
                            }
                        }

                    }
                    headerText = sr.ReadLine();
                    if (!string.IsNullOrEmpty(headerText))
                    {


                        string[] headers = headerText.Split(Convert.ToChar(dr["PC_DELIMITER"].ToString()));
                        if (headers.Length == (int)dr["PC_FIELDCOUNT"])
                        {
                            if (!string.IsNullOrEmpty(dr["PC_FIRSTFIELDNAME"].ToString()))
                            {
                                if (dr["PC_FIRSTFIELDNAME"].ToString() == headers[0])
                                {
                                    if (!string.IsNullOrEmpty(dr["PC_LASTFIELDNAME"].ToString()))
                                    {
                                        if (dr["PC_LASTFIELDNAME"].ToString() == headers[headers.Length - 1])
                                        {
                                            return (Guid)dr["PC_P"];
                                        }

                                    }
                                    else
                                    {
                                        return (Guid)dr["PC_P"];
                                    }

                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Exception found. Error was " + ex.Message, System.Drawing.Color.Red);
                    MailModule.sendMsg(fileName, Globals.glAlertsTo, "Unhandled Exception found", ex);
                }


            }
            return result;
        }
        private ProcessResult ProcessCustomFiles(String xmlFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;


            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            TransReference trRef = new TransReference();

            switch (processingFile.Extension.ToUpper())
            {
                case ".XML":

                    thisResult = ProcessCargowiseFiles(xmlFile);

                    break;
                case ".CSV":
                    break;

            }

            return thisResult;
        }

        private ProcessResult ProcessUDM(string xmlfile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            CW.UniversalInterchange cwFile = new CW.UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(CW.UniversalInterchange));
                cwFile = (CW.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            if (cwFile.Body.BodyField == null)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise File found but is not UDM. Attempting NDM: ");
                //thisResult = ProcessProductResponse(xmlfile);
                return thisResult;
            }
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            TransReference trRef = new TransReference();
            List<CW.DataSource> dscoll = new List<CW.DataSource>();
            CW.DataContext dc = new CW.DataContext();
            dc = cwFile.Body.BodyField.Shipment.DataContext;

            if (dc.DataSourceCollection != null)
            {
                try
                {
                    //dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                    dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                }
                catch (Exception)
                {

                    NodeResources.AddText(rtbLog, "XML File Missing DataSourceCollection tag");
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e111;
                    error.Description = "DataSource Collection not found:";
                    error.Severity = "Fatal";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = xmlfile;
                    procerror.ProcId = Guid.Empty;

                    //AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.glFailPath;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }

            }
            try
            {
                cwRx++;
                NodeResources.AddLabel(lblCwRx, cwRx.ToString());
                senderID = cwFile.Header.SenderID;
                recipientID = cwFile.Header.RecipientID;
                if (cwFile.Body.BodyField.Shipment.DataContext.EventType != null)
                {
                    eventCode = cwFile.Body.BodyField.Shipment.DataContext.EventType.Code;
                }
                else
                {
                    eventCode = string.Empty;
                }
                if (cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose != null)
                {
                    reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code;
                }
                else
                {
                    reasonCode = string.Empty;
                }
                //reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code;
                CustProfileRecord custProfileRecord = new CustProfileRecord();
                custProfileRecord = GetCustomerProfile(senderID, recipientID, reasonCode, eventCode);


                if (custProfileRecord.C_name != null)
                {
                    //Is Client Active and not on hold
                    if (custProfileRecord.C_is_active == "N" | custProfileRecord.C_on_hold == "Y")
                    {

                        string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_path, "Held"));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        { xmlfile = f; }
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                    else
                    {
                        Globals.ArchiveFile(Globals.glArcLocation, xmlfile);
                        switch (custProfileRecord.P_MsgType.Trim())
                        {
                            case "Conversion":// Rename the Cargowise XML File to the new Global XML
                                thisResult = ConvertCWFile(custProfileRecord, xmlfile);
                                break;
                            case "Response":
                                thisResult = SendResponse(cwFile, custProfileRecord);
                                thisResult.FileName = xmlfile;
                                break;

                        }
                        return thisResult;
                    }
                }

            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
            }
            return thisResult;

        }

        private ProcessResult SendResponse(UniversalInterchange cwFile, CustProfileRecord custProfileRecord)
        {
            ProcessResult result = new ProcessResult();
            string msgBody = string.Empty;
            msgBody = "Confirmation that shipment details have been imported into Evans International" + Environment.NewLine;
            msgBody += "Shipment Details: " + Environment.NewLine;
            if (cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection[0].Type == "ForwardingConsol")
            {
                Shipment consol = cwFile.Body.BodyField.Shipment;
                if (consol.WayBillType.Code == "MWB")
                {
                    if (!string.IsNullOrEmpty(consol.WayBillNumber))
                    {
                        msgBody += "MasterBill Number: " + consol.WayBillNumber;

                    }
                    msgBody += "Evans Consol Reference: " + consol.DataContext.DataSourceCollection[0].Key + Environment.NewLine;
                    msgBody += (string.IsNullOrEmpty(consol.AgentsReference) ? "Agent Reference not Found" : "MacNels Reference: " + consol.AgentsReference) + Environment.NewLine;
                    msgBody += (string.IsNullOrEmpty(consol.VesselName) ? "Vessel Name not found" : "Vessel/Voyage: " + consol.VesselName + " /" + consol.VoyageFlightNo) + Environment.NewLine;
                    msgBody += "HouseBills attached: " + Environment.NewLine;
                    foreach (Shipment shipment in consol.SubShipmentCollection)
                    {
                        msgBody += "HBL:\t" + shipment.WayBillNumber + "\t (Evans Shipment Number: \t" + shipment.DataContext.DataSourceCollection[0].Key + ")" + Environment.NewLine;
                    }

                }
            }
            result.Processed = true;
            MailModule.sendMsg("", custProfileRecord.P_EmailAddress, custProfileRecord.P_Subject, msgBody);
            return result;

        }

        private ProcessResult ConvertCWFile(CustProfileRecord cust, String file)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Beginning Cargowise Conversion: " + file);


            if (!String.IsNullOrEmpty(cust.P_Method))
            {
                string custMethod;
                custMethod = cust.P_Method;
                string custParams = cust.P_Params;
                Type custType = this.GetType();
                MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                try
                {
                    var varResult = custMethodToRun.Invoke(this, new Object[] { file, cust });
                    if (varResult != null)
                    {
                        //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);

                        result.FileName = file;
                        result.Processed = true;
                    }
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    strEx += " " + ex.Message;
                }


            }
            return result;


        }

        private ProcessResult ProcessCargowiseFiles(string xmlfile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlfile);
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File Found " + Path.GetFileName(xmlfile) + ". Checking for Cargowise File.");
            String ns = NodeResources.GetMessageNamespace(xmlfile);
            String appCode = NodeResources.GetApplicationCode(ns);

            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            ProcessResult canDelete = new ProcessResult();
            switch (appCode)
            {

                case "UDM":
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (UDM) XML File :" + xmlfile + ". " + "");
                    if (NodeResources.GetXMLType(xmlfile) == "UniversalEvent")
                    {
                        //canDelete = ProcessUDM(xmlfile);
                    }
                    else
                    {
                        canDelete = ProcessUDM(xmlfile);
                    }
                    break;
                case "NDM":
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (NDM) XML File :" + xmlfile + ". " + "");
                    //  canDelete = ProcessProductResponse(xmlfile);
                    break;
                default:
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is not a valid Cargowise XML File :" + xmlfile + ". Checking Client Specific formats" + "");
                    canDelete = ProcessCustomXML(xmlfile);
                    break;
            }

            if (canDelete.Processed)
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                File.Delete(canDelete.FileName);
            }
            else
            {
                if (canDelete.FileName != null)
                {
                    string f = NodeResources.MoveFile(xmlfile, canDelete.FolderLoc);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }
                }
            }
            return canDelete;
        }

        private void GetProcErrors()
        {
            SqlConnection sqlConn = new SqlConnection();
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            try
            {
                sqlConn.Open();
                SqlDataAdapter daProcErrors = new SqlDataAdapter("SELECT E_P, P_DESCRIPTION, E_SENDERID, E_RECIPIENTID, E_PROCDATE, E_FILENAME, E_ERRORDESC, E_ERRORCODE, " +
                                                                "E_PK, P_PATH FROM VW_ProcessngErrors ORDER BY E_PROCDATE", sqlConn);
                DataSet dsProcErrors = new DataSet();
                dgProcessingErrors.AutoGenerateColumns = false;
                daProcErrors.Fill(dsProcErrors, "PROCERRORS");
                dgProcessingErrors.DataSource = dsProcErrors;
                dgProcessingErrors.DataMember = "PROCERRORS";
                sqlConn.Close();
            }
            catch (Exception)
            {

            }

        }


        private string FixNumber(string bl)
        {
            string result;
            if (bl.Contains("/"))
            {
                result = bl.Replace("/", "-");
            }
            else
            {
                result = bl;
            }

            return result;
        }

        private ProcessResult ProcessCustomXML(string XMLFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());

            ProcessResult result = new ProcessResult();
            Globals.ArchiveFile(Globals.glArcLocation, XMLFile);
            result = SeperateXSDfromXML(XMLFile);

            return result;

        }

        private ProcessResult SeperateXSDfromXML(string XMLFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();
            StreamReader sr = new StreamReader(XMLFile);
            string xmlNameSpace = string.Empty;
            string currentLine = string.Empty;
            bool isMacnelsSG = false;

            while (!isMacnelsSG)
            {
                currentLine = sr.ReadLine();
                if (currentLine == null) break;
                if (currentLine.Contains("Sebl1"))
                {
                    isMacnelsSG = true;
                    //xmlNameSpace = @"<xs:schema id=""NewDataSet"" xmlns="""" xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:msdata=""urn:schemas-microsoft-com:xml-msdata"">
                    xmlNameSpace = @"<?xml version=""1.0"" standalone=""yes""?><NewDataSet>";
                }
            }

            //while (!currentLine.Contains(@"<xs:schema id=""NewDataSet"" xmlns="""" xmlns:xs=""http://www.w3.org/2001/XMLSchema"" xmlns:msdata=""urn:schemas-microsoft-com:xml-msdata"">")|| currentLine!=null)
            //{
            //    xmlNameSpace += currentLine;
            //    currentLine = sr.ReadLine();
            //    if (currentLine == null)
            //        break;

            //}


            if (isMacnelsSG)
            {
                string xsdFile = @"<?xml version=""1.0"" ?>" + Environment.NewLine + currentLine + Environment.NewLine;

                while (!currentLine.Contains(@"</xs:schema>"))
                {
                    currentLine = sr.ReadLine();
                    xsdFile += currentLine + Environment.NewLine;
                }
                string schemaFile = Path.Combine(Globals.glLibPath, "MacNells.XSD");
                if (!File.Exists(schemaFile))
                {
                    using (var newStream = NodeResources.CreateStreamFromString(xsdFile))
                    {
                        using (System.IO.FileStream outputFile = new FileStream(schemaFile, FileMode.Create))
                        {

                            newStream.CopyTo(outputFile);
                            outputFile.Close();
                        }

                    }
                }
                string tempXMLFile = xmlNameSpace + Environment.NewLine + sr.ReadToEnd();
                NewDataSet macnelsDataSet = NodeResources.CreateMacnells(tempXMLFile);
                if (macnelsDataSet != null)
                {
                    ConvertToCWFile(macnelsDataSet);
                }
                result.FileName = XMLFile;
                result.Processed = true;
            }
            else
            {
                ProcessUnknownXML(XMLFile);
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + XMLFile + " is not a Known filetype. Ignoring.", Color.Orange);
                result.FileName = XMLFile;
                result.Processed = true;
            }
            return result;
        }

        private void ProcessUnknownXML(string xMLFile)
        {

            XDocument xDoc = XDocument.Load(xMLFile);
            var header = from mnlkr in xDoc.Descendants("HEADER") select mnlkr;




        }

        private string GetXmlEncoding(string xmlString)
        {
            string result = string.Empty;
            using (var stringReader = new StringReader(xmlString))
            {
                var settings = new XmlReaderSettings { ConformanceLevel = ConformanceLevel.Fragment };

                using (var xmlReader = XmlReader.Create(stringReader, settings))
                {
                    if (!xmlReader.Read())
                    {
                        throw new ArgumentException(
                        "The provided XML string does not contain enough data to be valid XML (see https://msdn.microsoft.com/en-us/library/system.xml.xmlreader.read)");
                    }

                    result = xmlReader.GetAttribute("encoding");
                    return result;
                }
            }

            return result;
        }

        private void ConvertToCWFile(NewDataSet macnelsDataSet)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            CW.DataContext consolDataContext = new CW.DataContext();
            CW.Company company = new CW.Company
            {
                Code = "SYD",
                Name = Globals.gl_Customer,
                Country = new CW.Country { Code = "AU", Name = "Australia" }

            };
            List<CW.DataTarget> dataConsolTargetColl = new List<CW.DataTarget>();
            CW.DataTarget dataConsolTarget = new CW.DataTarget();
            CW.CodeDescriptionPair transMode = new CW.CodeDescriptionPair();
            CW.CodeDescriptionPair paymentMode = new CW.CodeDescriptionPair();
            CW.CodeDescriptionPair cdPlace = new CW.CodeDescriptionPair();
            CW.WayBillType wayBill = new CW.WayBillType();
            CW.Date date = new CW.Date();
            CW.Shipment consol = new CW.Shipment();
            List<Shipment> shipments = new List<Shipment>();
            string mbl = string.Empty;
            foreach (var v in macnelsDataSet.Items)
            {
                if (v.GetType() == typeof(NewDataSetSebl1))
                {
                    NewDataSetSebl1 sebl1 = (NewDataSetSebl1)v;
                    Shipment shipment = new Shipment();
                    switch (sebl1.ShipmentType)
                    {
                        case "M": //Master
                            consol = CreateConsol(sebl1);
                            mbl = consol.WayBillNumber;
                            consol.ContainerCollection = new ShipmentContainerCollection();
                            consol.ContainerCollection = CreateContainerList(macnelsDataSet, sebl1.TrxNo);
                            break;
                        case "H": //Shipment
                            shipment = new Shipment();
                            shipment = CreateShipment(sebl1);
                            shipment.PackingLineCollection = CreatePackingLineList(macnelsDataSet, sebl1.TrxNo, sebl1.OBlNo).ToArray();
                            shipments.Add(shipment);

                            break;
                        case "S": // Sub House
                            Shipment subShipment = new Shipment();
                            subShipment = CreateShipment(sebl1);
                            var ar = subShipment.AdditionalReferenceCollection.AdditionalReference.ToList().Find(x => x.Type.Code == "UCR");
                            foreach (Shipment s in shipments)
                            {
                                var addRef = s.AdditionalReferenceCollection.AdditionalReference.ToList().Find(x => x.Type.Code == "UCR");
                                if (ar.ReferenceNumber == addRef.ReferenceNumber)
                                {
                                    List<Shipment> subShipments = new List<Shipment>();
                                    subShipments.Add(subShipment);
                                    s.SubShipmentCollection = subShipments.ToArray();
                                    break;
                                }
                            }
                            break;
                    }
                    consol.SubShipmentCollection = shipments.ToArray();
                }
            }

            CW.UniversalShipmentData bodyField = new CW.UniversalShipmentData();
            bodyField.Shipment = consol;
            CW.UniversalInterchangeBody body = new CW.UniversalInterchangeBody();
            body.BodyField = bodyField;
            CW.UniversalInterchangeHeader header = new CW.UniversalInterchangeHeader
            {
                RecipientID = Globals.glCustCode,
                SenderID = "MACNELSSG"
            };
            CW.UniversalInterchange interchange = new CW.UniversalInterchange();
            interchange.Body = body;
            interchange.version = "1.1";
            interchange.Header = header;

            String cwXML = Path.Combine(Globals.glOutputDir, Globals.glCustCode + "-" + FixNumber(mbl) + ".xml");
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(Globals.glOutputDir, Globals.glCustCode + "-" + FixNumber(mbl) + iFileCount + ".xml");
            }
            Stream outputCW = File.Open(cwXML, FileMode.Create);
            StringWriter writer = new StringWriter();
            interchange.Body = body;
            XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
            var cwNSUniversal = new XmlSerializerNamespaces();
            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            xSer.Serialize(outputCW, interchange, cwNSUniversal);
            outputCW.Flush();
            outputCW.Close();
            Globals.ArchiveFile(Globals.glArcLocation, cwXML);
        }

        private List<PackingLine> CreatePackingLineList(NewDataSet macnelsDataSet, int trxNo, string oblNo)
        {
            List<PackingLine> result = new List<PackingLine>();
            foreach (var v in macnelsDataSet.Items)
            {
                if (v.GetType() == typeof(NewDataSetSebl2))
                {
                    NewDataSetSebl2 sebl2 = (NewDataSetSebl2)v;
                    if (sebl2.TrxNo == trxNo)
                    {
                        PackingLine pl = new PackingLine();
                        pl.Link = sebl2.LineItemNo;
                        pl.LinkSpecified = true;
                        pl.ContainerLink = GetContaineLink(macnelsDataSet, sebl2.ContainerNo, oblNo);

                        pl.ContainerLinkSpecified = true;
                        pl.ContainerNumber = sebl2.ContainerNo;
                        pl.Weight = sebl2.GrossWeight;
                        pl.WeightSpecified = true;
                        pl.WeightUnit = new UnitOfWeight { Code = "KG" };
                        pl.PackQty = sebl2.Pcs;
                        pl.PackQtySpecified = true;
                        pl.PackType = new PackageType { Code = sebl2.UomCode, Description = sebl2.UomDescription };

                        string description = string.Empty;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription01) ? string.Empty : sebl2.GoodsDescription01 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription02) ? string.Empty : sebl2.GoodsDescription02 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription03) ? string.Empty : sebl2.GoodsDescription03 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription04) ? string.Empty : sebl2.GoodsDescription04 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription05) ? string.Empty : sebl2.GoodsDescription05 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription06) ? string.Empty : sebl2.GoodsDescription06 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription07) ? string.Empty : sebl2.GoodsDescription07 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription08) ? string.Empty : sebl2.GoodsDescription08 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription09) ? string.Empty : sebl2.GoodsDescription09 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription10) ? string.Empty : sebl2.GoodsDescription10 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription11) ? string.Empty : sebl2.GoodsDescription11 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription12) ? string.Empty : sebl2.GoodsDescription12 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription13) ? string.Empty : sebl2.GoodsDescription13 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription14) ? string.Empty : sebl2.GoodsDescription14 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription15) ? string.Empty : sebl2.GoodsDescription15 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription16) ? string.Empty : sebl2.GoodsDescription16 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription17) ? string.Empty : sebl2.GoodsDescription17 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription18) ? string.Empty : sebl2.GoodsDescription18 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription19) ? string.Empty : sebl2.GoodsDescription19 + Environment.NewLine;
                        description += string.IsNullOrEmpty(sebl2.GoodsDescription20) ? string.Empty : sebl2.GoodsDescription20 + Environment.NewLine;
                        pl.DetailedDescription = description;
                        string marksNos = string.Empty;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo01) ? string.Empty : sebl2.MarkNo01 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo02) ? string.Empty : sebl2.MarkNo02 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo03) ? string.Empty : sebl2.MarkNo03 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo04) ? string.Empty : sebl2.MarkNo04 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo05) ? string.Empty : sebl2.MarkNo05 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo06) ? string.Empty : sebl2.MarkNo06 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo07) ? string.Empty : sebl2.MarkNo07 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo08) ? string.Empty : sebl2.MarkNo08 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo09) ? string.Empty : sebl2.MarkNo09 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo10) ? string.Empty : sebl2.MarkNo10 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo11) ? string.Empty : sebl2.MarkNo11 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo12) ? string.Empty : sebl2.MarkNo12 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo13) ? string.Empty : sebl2.MarkNo13 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo14) ? string.Empty : sebl2.MarkNo14 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo15) ? string.Empty : sebl2.MarkNo15 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo16) ? string.Empty : sebl2.MarkNo16 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo17) ? string.Empty : sebl2.MarkNo17 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo18) ? string.Empty : sebl2.MarkNo18 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo19) ? string.Empty : sebl2.MarkNo19 + Environment.NewLine;
                        marksNos += string.IsNullOrEmpty(sebl2.MarkNo20) ? string.Empty : sebl2.MarkNo20 + Environment.NewLine;
                        pl.MarksAndNos = marksNos;
                        pl.Volume = sebl2.Volume;
                        pl.VolumeSpecified = true;
                        pl.VolumeUnit = new UnitOfVolume { Code = "M3" };
                        result.Add(pl);
                    }
                }

            }
            return result;
        }

        private int GetContaineLink(NewDataSet macnelsDataSet, string containerNo, string oblNo)
        {
            int result = 0;
            foreach (var V in macnelsDataSet.Items)
            {
                if (V.GetType() == typeof(NewDataSetSebl2))
                {
                    NewDataSetSebl2 cont = (NewDataSetSebl2)V;

                    if (cont.ContainerNo == containerNo || cont.BlNo == oblNo)
                    {
                        result = cont.LineItemNo;
                    }


                }
            }

            return result;
        }

        private ShipmentContainerCollection CreateContainerList(NewDataSet macNellsDataSet, int trxNo)
        {
            ShipmentContainerCollection result = new ShipmentContainerCollection();
            result.Content = CollectionContent.Partial;
            result.ContentSpecified = true;
            List<Container> containers = new List<Container>();

            foreach (var v in macNellsDataSet.Items)
            {

                if (v.GetType() == typeof(NewDataSetSebl2))
                {
                    Container container = new Container();
                    NewDataSetSebl2 sebl2 = (NewDataSetSebl2)v;
                    if (sebl2.TrxNo == trxNo)
                    {
                        container.ContainerNumber = sebl2.ContainerNo;
                        container.ContainerType = new ContainerContainerType { Code = sebl2.ContainerType };
                        container.Seal = sebl2.SealNo;
                        container.Link = sebl2.LineItemNo;
                        container.LinkSpecified = true;
                        container.FCL_LCL_AIR = new ContainerMode { Code = "GRP" };
                        containers.Add(container);
                    }
                }
            }
            result.Container = containers.ToArray();
            return result;
        }

        private Shipment CreateShipment(NewDataSetSebl1 sebl1)
        {
            Shipment result = new Shipment();
            List<DataTarget> dataTargets = new List<DataTarget>();
            DataTarget dt = new DataTarget { Type = "ForwardingShipment" };
            dataTargets.Add(dt);
            result.DataContext = new DataContext { DataTargetCollection = dataTargets.ToArray() };


            result.WayBillNumber = sebl1.BlNo;
            result.WayBillType = new WayBillType { Code = "HWB" };
            result.ContainerMode = new ContainerMode { Code = sebl1.CargoType };
            result.ShipmentType = new CodeDescriptionPair { Code = "STD" };

            result.TotalVolumeUnit = new UnitOfVolume { Code = "M3" };
            result.TotalWeightUnit = new UnitOfWeight { Code = "KG" };
            result.TransportMode = new CodeDescriptionPair { Code = "SEA" };
            //TODO
            result.ContainerMode = new ContainerMode { Code = sebl1.CargoType };
            //Change the type of shipment based upon the datafile.             
            result.GoodsDescription = sebl1.CommodityDescription;
            result.FreightRateCurrency = new Currency { Code = sebl1.CurrCode };
            //result.FreightRate = sebl1.CurrRate;
            //result.FreightRateSpecified = true;
            result.NoOriginalBills = (sbyte)sebl1.NoOfOriginBl;
            result.NoOriginalBillsSpecified = true;
            result.PortOfOrigin = new UNLOCO { Code = sebl1.PortOfLoadingCode };
            result.PortOfDestination = new UNLOCO { Code = sebl1.PortOfDischargeCode };
            result.ActualChargeable = sebl1.TotalChargeWeight;
            result.ActualChargeableSpecified = true;
            result.TotalWeight = sebl1.TotalGrossWeight;
            result.TotalWeightSpecified = true;
            result.OuterPacks = sebl1.TotalPcs;
            result.OuterPacksSpecified = true;
            result.TotalVolume = sebl1.TotalVolume;
            result.TotalVolumeSpecified = true;
            result.OuterPacksPackageType = new PackageType { Code = sebl1.UomCode, Description = sebl1.UomDescription };
            List<Date> dates = new List<Date>();
            Date date = new Date { Type = DateType.BillIssued, IsEstimate = false, IsEstimateSpecified = true, Value = ConvertDate(sebl1.BlIssueDate) };
            dates.Add(date);
            date = new Date { Type = DateType.Arrival, IsEstimate = true, IsEstimateSpecified = true, Value = ConvertDate(sebl1.EtaDate) };
            dates.Add(date);
            date = new Date { Type = DateType.Departure, IsEstimate = true, IsEstimateSpecified = true, Value = ConvertDate(sebl1.EtdDate) };
            dates.Add(date);
            date = new Date { Type = DateType.ShippedOnBoard, IsEstimate = false, IsEstimateSpecified = true, Value = ConvertDate(sebl1.LadenDate) };
            result.DateCollection = dates.ToArray();

            List<OrganizationAddress> organizationAddresses = new List<OrganizationAddress>();

            OrganizationAddress orgCnee = new OrganizationAddress
            {
                AddressType = "ConsigneeDocumentaryAddress",
                CompanyName = sebl1.ConsigneeName,
                Address1 = sebl1.ConsigneeAddress1,
                Address2 = sebl1.ConsigneeAddress2,
                OrganizationCode = string.IsNullOrEmpty(sebl1.ConsigneeAccCode) ? sebl1.ConsigneeName : sebl1.ConsigneeAccCode

            };
            organizationAddresses.Add(orgCnee);
            if (!string.IsNullOrEmpty(sebl1.NotifyName))
            {
                OrganizationAddress orgNotify = new OrganizationAddress
                {
                    AddressType = "NotifyParty",
                    CompanyName = sebl1.NotifyName,
                    Address1 = sebl1.NotifyAddress1,
                    Address2 = sebl1.NotifyAddress2,
                    OrganizationCode = string.IsNullOrEmpty(sebl1.NotifyAcctCode) ? sebl1.NotifyName : sebl1.NotifyAcctCode
                };
                // organizationAddresses.Add(orgNotify);
            }
            if (!string.IsNullOrEmpty(sebl1.ShipperName))
            {
                OrganizationAddress orgNCnor = new OrganizationAddress
                {
                    AddressType = "ConsignorDocumentaryAddress",
                    CompanyName = sebl1.ShipperName,
                    Address1 = sebl1.ShipperAddress1,
                    Address2 = sebl1.ShipperAddress2,
                    OrganizationCode = string.IsNullOrEmpty(sebl1.ShipperAccCode) ? sebl1.ShipperName : sebl1.ShipperAccCode
                };
                organizationAddresses.Add(orgNCnor);
            }
            result.OrganizationAddressCollection = organizationAddresses.ToArray();
            List<AdditionalReference> additionalReferences = new List<AdditionalReference>();
            ShipmentAdditionalReferenceCollection shipmentAdditionalReferenceCollection = new ShipmentAdditionalReferenceCollection();
            AdditionalReference additionalReference = new AdditionalReference();
            if (sebl1.JobNo != null)
            {
                additionalReference = new AdditionalReference { Type = new EntryType { Code = "UCR" }, ReferenceNumber = sebl1.JobNo };
                additionalReferences.Add(additionalReference);
            }
            shipmentAdditionalReferenceCollection.Content = CollectionContent.Partial;
            shipmentAdditionalReferenceCollection.ContentSpecified = true;
            if (sebl1.HouseJobNo != null)
            {
                additionalReference = new AdditionalReference { Type = new EntryType { Code = "UCR" }, ReferenceNumber = sebl1.HouseJobNo };
                additionalReferences.Add(additionalReference);
            }
            additionalReference = new AdditionalReference { Type = new EntryType { Code = "BKG" }, ReferenceNumber = sebl1.BookingNo };
            additionalReferences.Add(additionalReference);
            shipmentAdditionalReferenceCollection.AdditionalReference = additionalReferences.ToArray();
            result.AdditionalReferenceCollection = shipmentAdditionalReferenceCollection;
            ShipmentNoteCollection shipmentNoteCollection = new ShipmentNoteCollection();
            shipmentNoteCollection.Content = CollectionContent.Partial;
            List<Note> notes = new List<Note>();
            string noteText = string.Empty;
            noteText += string.IsNullOrEmpty(sebl1.DeliveryInstruction1) ? string.Empty : sebl1.DeliveryInstruction1 + Environment.NewLine;
            noteText += string.IsNullOrEmpty(sebl1.DeliveryInstruction2) ? string.Empty : sebl1.DeliveryInstruction2 + Environment.NewLine;
            noteText += string.IsNullOrEmpty(sebl1.DeliveryInstruction3) ? string.Empty : sebl1.DeliveryInstruction3 + Environment.NewLine;
            noteText += string.IsNullOrEmpty(sebl1.DeliveryInstruction4) ? string.Empty : sebl1.DeliveryInstruction4 + Environment.NewLine;
            noteText += string.IsNullOrEmpty(sebl1.DeliveryInstruction5) ? string.Empty : sebl1.DeliveryInstruction5 + Environment.NewLine;
            noteText += string.IsNullOrEmpty(sebl1.DeliveryInstruction6) ? string.Empty : sebl1.DeliveryInstruction6 + Environment.NewLine;
            noteText += string.IsNullOrEmpty(sebl1.DeliveryInstruction7) ? string.Empty : sebl1.DeliveryInstruction7 + Environment.NewLine;
            noteText += string.IsNullOrEmpty(sebl1.DeliveryInstruction8) ? string.Empty : sebl1.DeliveryInstruction8 + Environment.NewLine;
            if (!string.IsNullOrEmpty(noteText))
            {
                Note note = new Note { Description = "Import Delivery Instructions", NoteText = noteText };
                notes.Add(note);
                shipmentNoteCollection.Note = notes.ToArray();
                result.NoteCollection = shipmentNoteCollection;
            }
            result.ReleaseType = new CodeDescriptionPair { Code = "OBL" };

            return result;
        }

        private Shipment CreateConsol(NewDataSetSebl1 sebl1)
        {
            Shipment result = new Shipment();
            result.DataContext = new DataContext
            {
                Company = new Company
                {
                    Code = "NE1",
                    Country = new Country
                    {
                        Code = "AU",
                        Name = "Australia"
                    }

                },
                DataProvider = "MACNELSSG",
                EnterpriseID = "EFC",
                ServerID = "BNE",
            };
            List<DataTarget> dataTargets = new List<DataTarget>();
            DataTarget dt = new DataTarget { Type = "ForwardingConsol" };
            dataTargets.Add(dt);
            result.DataContext.DataTargetCollection = dataTargets.ToArray();
            result.AgentsReference = sebl1.MasterJobNo;
            result.FreightRateCurrency = new Currency { Code = sebl1.CurrCode };

            if (sebl1.FrtPpCcFlag == "P")
            {
                result.PaymentMethod = new CodeDescriptionPair { Code = "PPD" };
            }
            else
            {
                result.PaymentMethod = new CodeDescriptionPair { Code = "CCX" };

            }
            result.NoOriginalBills = (sbyte)sebl1.NoOfOriginBl;
            result.NoCopyBillsSpecified = true;
            result.WayBillNumber = sebl1.OBlNo;
            result.WayBillType = new WayBillType { Code = "MWB" };
            result.ShipmentType = new CodeDescriptionPair { Code = "AGT" };
            switch (sebl1.CargoType)
            {
                case "CON": // consolidated = Groupage
                    result.ContainerMode = new ContainerMode { Code = "GRP" };
                    break;
                case "FCL":
                    result.ContainerMode = new ContainerMode { Code = "FCL" };
                    break;
                case "AGT":
                    result.ContainerMode = new ContainerMode { Code = "AGT" };
                    break;
            }

            result.TransportMode = new CodeDescriptionPair { Code = "SEA" };
            OrganizationAddress organizationAddress = new OrganizationAddress
            {
                AddressType = "SendingForwarderAddress",
                OrganizationCode = "MACNELSSG"
            };
            List<OrganizationAddress> organizationAddresses = new List<OrganizationAddress>();
            organizationAddresses.Add(organizationAddress);
            organizationAddress = new OrganizationAddress
            {
                AddressType = "ReceivingForwarderAddress",
                OrganizationCode = "EIFC"
            };
            organizationAddresses.Add(organizationAddress);
            organizationAddress = new OrganizationAddress
            {
                AddressType = "ShippingLineAddress",
                CompanyName = sebl1.ShippingLineName,
                OrganizationCode = sebl1.ShippinglineCode
            };
            organizationAddresses.Add(organizationAddress);
            result.OrganizationAddressCollection = organizationAddresses.ToArray();
            result.PortOfDischarge = new UNLOCO { Code = sebl1.PortOfDischargeCode };
            result.PortOfLoading = new UNLOCO { Code = sebl1.PortOfLoadingCode };
            result.TotalVolumeUnit = new UnitOfVolume { Code = "M3" };
            result.TotalWeightUnit = new UnitOfWeight { Code = "KG" };
            List<TransportLeg> transportLegs = new List<TransportLeg>();
            TransportLeg originLeg = new TransportLeg
            {
                LegOrder = 1,
                LegType = TransportLegLegType.Main,
                LegTypeSpecified = true,
                TransportMode = TransportLegTransportMode.Sea,
                TransportModeSpecified = true,
                EstimatedArrival = ConvertDate(sebl1.EtaDate),
                EstimatedDeparture = ConvertDate(sebl1.EtdDate),
                PortOfDischarge = new UNLOCO { Code = sebl1.PortOfDischargeCode },
                PortOfLoading = new UNLOCO { Code = sebl1.PortOfLoadingCode },
                VesselName = sebl1.VesselName,
                VoyageFlightNo = sebl1.VoyageNo
            };
            transportLegs.Add(originLeg);
            result.TransportLegCollection = transportLegs.ToArray();
            return result;
        }

        private string ConvertDate(DateTime etaDate)
        {
            DateTime localDate = new DateTime();
            string result = string.Empty;
            if (etaDate != null)
            {
                localDate = DateTime.Parse(etaDate.ToString("d"));
                result = localDate.ToString("s");
            }
            return result;

        }

        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnums Enums = new frmEnums();
            Enums.Show();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private CustProfileRecord GetCustomerProfile(string senderID, string recipientID, string reasonCode, string eventCode)
        {
            CustProfileRecord result = new CustProfileRecord();

            SqlCommand sqlCustProfile = new SqlCommand("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, P_EVENTCODE, " +
                                                             "C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, P_METHOD, P_PARAMLIST," +
                                                             "P_PATH, P_PASSWORD, P_MSGTYPE, P_RECIPIENTID, P_SENDERID, P_EMAILADDRESS, P_SUBJECT," +
                                                             "P_DESCRIPTION, P_BILLTO, P_PORT, P_DIRECTION, C_CODE, P_DTS, P_ACTIVE, P_SSL " +
                                                             "from vw_CustomerProfile WHERE P_SENDERID = @SENDERID and P_RECIPIENTID= @RECIPIENTID and P_ACTIVE='Y' " +
                                                             "Order by P_REASONCODE desc, P_EVENTCODE DESC ", sqlConn);

            sqlCustProfile.Parameters.AddWithValue("@SENDERID", senderID);
            sqlCustProfile.Parameters.AddWithValue("@RECIPIENTID", recipientID);

            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = Globals.connString();
            sqlConn.Open();
            List<CustProfileRecord> custProfile = new List<CustProfileRecord>();
            using (SqlDataReader drCustProfile = sqlCustProfile.ExecuteReader(CommandBehavior.CloseConnection))
            {
                if (drCustProfile.HasRows)
                {
                    while (drCustProfile.Read())
                    {
                        result = new CustProfileRecord();
                        result.C_id = (Guid)drCustProfile["C_ID"];
                        result.P_id = (Guid)drCustProfile["P_ID"];
                        result.C_name = drCustProfile["C_NAME"].ToString().Trim();
                        result.C_is_active = drCustProfile["C_IS_ACTIVE"].ToString().Trim();
                        result.C_on_hold = drCustProfile["C_ON_HOLD"].ToString().Trim();
                        result.C_path = drCustProfile["C_PATH"].ToString().Trim();
                        result.C_ftp_client = drCustProfile["C_FTP_CLIENT"].ToString().Trim();
                        result.P_reasoncode = drCustProfile["P_REASONCODE"].ToString().Trim();
                        result.P_EventCode = drCustProfile["P_EVENTCODE"].ToString().Trim();
                        result.P_server = drCustProfile["P_SERVER"].ToString().Trim();
                        result.P_username = drCustProfile["P_USERNAME"].ToString().Trim();
                        result.P_delivery = drCustProfile["P_DELIVERY"].ToString().Trim();
                        result.P_password = drCustProfile["P_PASSWORD"].ToString().Trim();
                        //result.P_recipientid = drCustProfile["P_RECIPIENTID"].ToString().Trim();
                        result.P_Senderid = drCustProfile["P_SENDERID"].ToString().Trim();
                        result.P_description = drCustProfile["P_DESCRIPTION"].ToString().Trim();
                        result.P_path = drCustProfile["P_PATH"].ToString().Trim();
                        result.P_port = drCustProfile["P_PORT"].ToString().Trim();
                        result.C_code = drCustProfile["C_CODE"].ToString().Trim();
                        result.P_BillTo = drCustProfile["P_BILLTO"].ToString().Trim();
                        result.P_Direction = drCustProfile["P_DIRECTION"].ToString().Trim();
                        result.P_MsgType = drCustProfile["P_MSGTYPE"].ToString().Trim();
                        result.P_Subject = drCustProfile["P_SUBJECT"].ToString().Trim();
                        result.P_EmailAddress = drCustProfile["P_EMAILADDRESS"].ToString().Trim();
                        result.P_Method = drCustProfile["P_METHOD"].ToString().Trim();
                        result.P_Params = drCustProfile["P_PARAMLIST"].ToString().Trim();
                        if (drCustProfile["P_SSL"].ToString().Trim() == "Y")
                        {
                            result.P_ssl = true;
                        }
                        else
                        {
                            result.P_ssl = false;
                        }

                        if (drCustProfile["P_DTS"].ToString().Trim() == "Y")
                        {
                            result.P_DTS = true;
                        }
                        else
                        {
                            result.P_DTS = false;
                        }
                        custProfile.Add(result);
                    }
                }
                else
                {
                    result = new CustProfileRecord();
                    SqlCommand execAdd = new SqlCommand();
                    execAdd.Connection = sqlConn;
                    execAdd.CommandText = "Add_Profile";
                    execAdd.CommandType = CommandType.StoredProcedure;
                    SqlParameter PC = execAdd.Parameters.Add("@P_C", SqlDbType.UniqueIdentifier);
                    PC.Value = getCustID(senderID);
                    SqlParameter PReasonCode = execAdd.Parameters.Add("@P_REASONCODE", SqlDbType.Char, 3);
                    PReasonCode.Value = reasonCode.ToUpper();
                    SqlParameter PEventCode = execAdd.Parameters.Add("@P_EVENTCODE", SqlDbType.Char, 3);
                    PEventCode.Value = eventCode.ToUpper();
                    SqlParameter PServer = execAdd.Parameters.Add("@P_SERVER", SqlDbType.VarChar, 50);
                    PServer.Value = "";
                    SqlParameter PUsername = execAdd.Parameters.Add("@P_USERNAME", SqlDbType.VarChar, 50);
                    PUsername.Value = "";
                    SqlParameter PPath = execAdd.Parameters.Add("@P_PATH", SqlDbType.VarChar, 100);
                    PPath.Value = "";
                    SqlParameter PPassword = execAdd.Parameters.Add("@P_PASSWORD", SqlDbType.VarChar, 50);
                    PPassword.Value = "";
                    string sDelivery = String.Empty;
                    sDelivery = "R";
                    SqlParameter pDelivery = execAdd.Parameters.Add("@P_DELIVERY", SqlDbType.Char, 1);
                    pDelivery.Value = sDelivery;
                    SqlParameter pMSGType = execAdd.Parameters.Add("@MSGTYPE", SqlDbType.VarChar, 20);
                    pMSGType.Value = "Original";
                    SqlParameter pBillType = execAdd.Parameters.Add("@BILLTO", SqlDbType.VarChar, 15);
                    pBillType.Value = senderID;
                    SqlParameter pChargeable = execAdd.Parameters.Add("@CHARGEABLE", SqlDbType.Char, 1);
                    pChargeable.Value = "Y";
                    SqlParameter PPort = execAdd.Parameters.Add("@P_PORT", SqlDbType.Char, 10);
                    PPort.Value = "";
                    SqlParameter PDescription = execAdd.Parameters.Add("@P_DESCRIPTION", SqlDbType.VarChar, 100);
                    PDescription.Value = "No Transport Profile found. Store in Folder";
                    SqlParameter PRecipientId = execAdd.Parameters.Add("@P_RECIPIENTID", SqlDbType.VarChar, 15);
                    PRecipientId.Value = recipientID;
                    SqlParameter PSenderID = execAdd.Parameters.Add("P_SENDERID", SqlDbType.VarChar, 15);
                    PSenderID.Value = senderID;
                    SqlParameter p_id = execAdd.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
                    SqlParameter PDirection = execAdd.Parameters.Add("@P_DIRECTION", SqlDbType.Char, 1);
                    PDirection.Value = "R";
                    p_id.Direction = ParameterDirection.Output;
                    SqlParameter r_Action = execAdd.Parameters.Add("@R_ACTION", SqlDbType.Char, 1);
                    r_Action.Direction = ParameterDirection.Output;
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Open();
                    execAdd.ExecuteNonQuery();
                    sqlConn.Close();
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Create New Profile Sender ID: " + senderID + " Recipient ID: " + recipientID + " Purpose Code: " + reasonCode + " Event Code:" + eventCode);

                }
                foreach (CustProfileRecord cp in custProfile)
                {
                    if (cp.P_reasoncode == reasonCode)
                    {
                        if (cp.P_EventCode == eventCode)
                        {
                            result = cp;
                            break;
                        }
                        else if (cp.P_EventCode == "*")
                        {
                            result = cp;
                            break;
                        }
                    }
                    else if (cp.P_reasoncode == "*")
                    {
                        if (cp.P_EventCode == eventCode)
                        {
                            result = cp;
                            break;
                        }
                        else if (cp.P_EventCode == "*")
                        {
                            result = cp;
                            break;
                        }
                    }

                }

            }
            return result;
        }


        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void eventTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEvents events = new frmEvents();
            events.Show();
        }

        private void eAdapterTestToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}

