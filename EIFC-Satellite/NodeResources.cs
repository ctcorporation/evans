﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using static EIFC_Satellite.SatelliteErrors;

namespace EIFC_Satellite
{
    class NodeResources
    {

        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    bool emptyCol = true;
                    for (int i = 0; i < dr.ItemArray.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(dr[i].ToString()))
                        {
                            emptyCol = false;
                        }
                    }
                    if (!emptyCol)
                    {
                        dt.Rows.Add(dr);
                    }

                }
            }
            sr.Close();

            return dt;


        }

        public static DataTable ConvertCSVtoDataTable(string strFilePath, int headerFrom, int dataFrom)

        {
            StreamReader sr = new StreamReader(strFilePath);
            for (int i = 0; i < dataFrom; i++)
            {
                sr.ReadLine();















            }
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {

                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            sr.Close();

            return dt;
        }

        public static CustProfileRecord GetCustProfileRecord(string senderID, string recipientID, string reasonCode, string eventCode)
        {

            CustProfileRecord result = new CustProfileRecord();


            using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
            {
                SqlCommand sqlCustProfile = new SqlCommand("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, P_EVENTCODE, " +
                                                                "C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, P_METHOD, P_PARAMLIST," +
                                                                "P_PATH, P_PASSWORD, P_MSGTYPE, P_Recipientid, P_SENDERID, P_EMAILADDRESS, P_SUBJECT," +
                                                                "P_DESCRIPTION, P_BILLTO, P_PORT, P_DIRECTION, C_CODE, P_DTS, P_ACTIVE, P_SSL " +
                                                                "from vw_CustomerProfile WHERE P_SENDERID = @SENDERID and P_Recipientid= @RECIPIENTID and P_ACTIVE='Y' " +
                                                                "Order by P_REASONCODE desc, P_EVENTCODE DESC ", sqlConn);

                sqlCustProfile.Parameters.AddWithValue("@SENDERID", senderID);
                sqlCustProfile.Parameters.AddWithValue("@RECIPIENTID", recipientID);
                sqlConn.Open();
                List<CustProfileRecord> custProfile = new List<CustProfileRecord>();
                using (SqlDataReader drCustProfile = sqlCustProfile.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (drCustProfile.HasRows)
                    {
                        while (drCustProfile.Read())
                        {
                            result = new CustProfileRecord();
                            result.C_id = (Guid)drCustProfile["C_ID"];
                            result.P_id = (Guid)drCustProfile["P_ID"];
                            result.C_name = drCustProfile["C_NAME"].ToString().Trim();
                            result.C_is_active = drCustProfile["C_IS_ACTIVE"].ToString().Trim();
                            result.C_on_hold = drCustProfile["C_ON_HOLD"].ToString().Trim();
                            result.C_path = drCustProfile["C_PATH"].ToString().Trim();
                            result.C_ftp_client = drCustProfile["C_FTP_CLIENT"].ToString().Trim();
                            result.P_reasoncode = drCustProfile["P_REASONCODE"].ToString().Trim();
                            result.P_EventCode = drCustProfile["P_EVENTCODE"].ToString().Trim();
                            result.P_server = drCustProfile["P_SERVER"].ToString().Trim();
                            result.P_username = drCustProfile["P_USERNAME"].ToString().Trim();
                            result.P_delivery = drCustProfile["P_DELIVERY"].ToString().Trim();
                            result.P_password = drCustProfile["P_PASSWORD"].ToString().Trim();
                            result.P_Recipientid = drCustProfile["P_Recipientid"].ToString().Trim();
                            result.P_Senderid = drCustProfile["P_SENDERID"].ToString().Trim();
                            result.P_description = drCustProfile["P_DESCRIPTION"].ToString().Trim();
                            result.P_path = drCustProfile["P_PATH"].ToString().Trim();
                            result.P_port = drCustProfile["P_PORT"].ToString().Trim();
                            result.C_code = drCustProfile["C_CODE"].ToString().Trim();
                            result.P_BillTo = drCustProfile["P_BILLTO"].ToString().Trim();
                            result.P_Direction = drCustProfile["P_DIRECTION"].ToString().Trim();
                            result.P_MsgType = drCustProfile["P_MSGTYPE"].ToString().Trim();
                            result.P_Subject = drCustProfile["P_SUBJECT"].ToString().Trim();
                            result.P_EmailAddress = drCustProfile["P_EMAILADDRESS"].ToString().Trim();
                            result.P_Method = drCustProfile["P_METHOD"].ToString().Trim();
                            result.P_Params = drCustProfile["P_PARAMLIST"].ToString().Trim();
                            if (drCustProfile["P_SSL"].ToString().Trim() == "Y")
                            {
                                result.P_ssl = true;
                            }
                            else
                            {
                                result.P_ssl = false;
                            }

                            if (drCustProfile["P_DTS"].ToString().Trim() == "Y")
                            {
                                result.P_DTS = true;
                            }
                            else
                            {
                                result.P_DTS = false;
                            }
                            custProfile.Add(result);
                        }
                    }
                    result = new CustProfileRecord();
                    foreach (CustProfileRecord cp in custProfile)
                    {
                        if (cp.P_reasoncode == reasonCode)
                        {
                            if (cp.P_EventCode == eventCode)
                            {
                                result = cp;
                                break;
                            }
                            else if (cp.P_EventCode == "*")
                            {
                                result = cp;
                                break;
                            }
                        }
                        else if (cp.P_reasoncode == "*")
                        {
                            if (cp.P_EventCode == eventCode)
                            {
                                result = cp;
                                break;
                            }
                            else if (cp.P_EventCode == "*")
                            {
                                result = cp;
                                break;
                            }
                        }
                    }
                }
            }
            return result;

        }

        public static string GetMessageNamespace(string messageFilePath)
        {
            using (var fileStream = GetFileAsStream(messageFilePath))
            {
                using (var reader = XmlReader.Create(fileStream))
                {
                    ReadToNextElement(reader);
                    return reader.NamespaceURI;
                }
            }
        }

        public static void ReadToNextElement(XmlReader reader)
        {
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    break;
                }
            }
        }

        public static string GetApplicationCode(string messageNamespace)
        {
            string appCode = string.Empty;
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    appCode = "UDM";
                    break;

                case "http://www.cargowise.com/Schemas/Native":
                    appCode = "NDM";
                    break;

                case "http://www.edi.com.au/EnterpriseService/":
                    appCode = "XMS";
                    break;

                default:
                    appCode = "";
                    break;
            }

            return appCode;


        }

        public static string GetXMLType(String messageFilePath)
        {
            string result = string.Empty;
            XDocument xDoc;
            using (var fileStream = GetFileAsStream(messageFilePath))
            {
                using (var reader = XmlReader.Create(fileStream))
                {
                    xDoc = XDocument.Load(reader);
                }
            }
            XNamespace ns = "http://www.cargowise.com/Schemas/Universal/2011/11";
            var xmlType = xDoc.Descendants().Where(n => n.Name == ns + "UniversalEvent").FirstOrDefault();
            if (xmlType != null)
            {

                result = xmlType.Name.LocalName;
            }
            return result;

        }

        public static string GetSchemaName(string messageNamespace)
        {
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Native":
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    return messageNamespace + "#UniversalInterchange";

                case "http://www.edi.com.au/EnterpriseService/":
                    return messageNamespace + "#XmlInterchange";

                default:
                    return messageNamespace;
            }
        }

        static Stream GetFileAsStream(string fileName)
        {
            return new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
        }

        public class CustomValidateHandler
        {
            private static Boolean isValid;
            private static IList<string> myValList = new List<string>();

            public IList<string> ValidationList
            {
                get { return myValList; }
                set { myValList = value; }
            }

            public Boolean IsValid
            {
                get { return isValid; }
                set { isValid = value; }
            }

            public static void HandlerErrors(Object sender, ValidationEventArgs e)
            {
                if (e.Severity == XmlSeverityType.Error)
                {
                    isValid = false;
                    myValList.Add(
                        String.Format(
                        Environment.NewLine + "Line: {0}, Position {1}: \"{2}\"",
                        e.Exception.LineNumber,
                        e.Exception.LinePosition,
                        e.Exception.Message));
                }

            }
        }
        public static NewDataSet CreateMacnells(string tempXMLFile)
        {
            NewDataSet macNellsDataSet = new NewDataSet();
            using (var xmlStream = NodeResources.CreateStreamFromString(tempXMLFile))
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                XmlSerializer macNelsImport = new XmlSerializer(typeof(NewDataSet), "");
                macNellsDataSet = (NewDataSet)macNelsImport.Deserialize(xmlStream);
                xmlStream.Close();
            }
            return macNellsDataSet;
        }

        public static Stream CreateStreamFromString(string str)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            return stream;


        }

        public static Guid GetCustomer(string custCode)
        {
            Guid result = Guid.Empty;
            SqlConnection sqlCTCConn = new SqlConnection
            {
                ConnectionString = Globals.CTCconnString()
            };
            SqlCommand sqlCust = new SqlCommand("Select C_ID FROM CUSTOMER WHERE C_CODE = @C_CODE", sqlCTCConn);

            sqlCust.Parameters.AddWithValue("@C_CODE", custCode);
            try
            {
                if (sqlCTCConn.State == ConnectionState.Open)
                {
                    sqlCTCConn.Close();
                }

                sqlCTCConn.Open();
                SqlDataReader dr = sqlCust.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr.HasRows)
                {
                    dr.Read();
                    result = (Guid)dr["C_ID"];

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error :" + ex.Message, "Error Locating Records", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;

        }
        public static void AddLabel(Label lb, string value)
        {
            if (lb.InvokeRequired)
            {
                lb.BeginInvoke(new System.Action(delegate { AddLabel(lb, value); }));
                return;
            }
            lb.Text = value;
        }

        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public bool LockedFile(FileInfo file)
        {
            try
            {
                string filePath = file.FullName;
                FileStream fs = File.OpenWrite(filePath);
                fs.Close();
                return false;
            }
            catch (Exception) { return true; }
        }

        public static void AddText(RichTextBox rtb, string value)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value); }));
                return;
            }

            rtb.AppendText((value) + Environment.NewLine);
            rtb.ScrollToCaret();

        }
        public static void AddText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        public static Guid FindTransactionID(TransReference tr)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            Guid transactionID = new Guid();
            SqlCommand sqlTransaction = new SqlCommand("returnTransactionID", sqlConn);
            sqlTransaction.CommandType = CommandType.StoredProcedure;
            SqlParameter tranId = sqlTransaction.Parameters.Add("@TID", SqlDbType.UniqueIdentifier);
            tranId.Direction = ParameterDirection.Output;
            SqlParameter ref1 = sqlTransaction.Parameters.Add("@Ref1", SqlDbType.NVarChar, 50);
            SqlParameter ref2 = sqlTransaction.Parameters.Add("@Ref2", SqlDbType.NVarChar, 50);
            SqlParameter ref3 = sqlTransaction.Parameters.Add("@Ref3", SqlDbType.NVarChar, 50);
            ref1.Value = tr.Reference1;
            if (String.IsNullOrEmpty(tr.Reference2))
            {
                ref2.Value = DBNull.Value;
            }
            else
            {
                ref2.Value = tr.Reference2;
            }

            if (String.IsNullOrEmpty(tr.Reference3))
            {
                ref3.Value = DBNull.Value;
            }
            else
            {
                ref3.Value = tr.Reference3;
            }

            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }

            sqlConn.Open();
            sqlTransaction.ExecuteNonQuery();
            if (tranId.Value == DBNull.Value)
            {
                transactionID = Guid.Empty;
            }
            else
            {
                transactionID = (Guid)tranId.Value;
            }

            return transactionID;
        }
        public static void AddProcessingError(ProcessingErrors procError)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            SqlCommand sqlProcErrors = new SqlCommand("Add_ProcessingError", sqlConn);
            sqlProcErrors.CommandType = CommandType.StoredProcedure;
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            SqlParameter senderID = sqlProcErrors.Parameters.Add("@SENDERID", SqlDbType.VarChar, 15);
            SqlParameter recipientID = sqlProcErrors.Parameters.Add("@RECIPIENTID", SqlDbType.VarChar, 15);
            SqlParameter filename = sqlProcErrors.Parameters.Add("@FILENAME", SqlDbType.VarChar, -1);
            SqlParameter procDate = sqlProcErrors.Parameters.Add("@ProcDate", SqlDbType.DateTime);
            SqlParameter errorCode = sqlProcErrors.Parameters.Add("@ERRORCODE", SqlDbType.NChar, 10);
            SqlParameter errorDesc = sqlProcErrors.Parameters.Add("@ERRORDESC", SqlDbType.VarChar, 200);
            SqlParameter procId = sqlProcErrors.Parameters.Add("@PROCID", SqlDbType.UniqueIdentifier);
            senderID.Value = procError.SenderId;
            recipientID.Value = procError.RecipientId;
            filename.Value = Path.GetFileName(procError.FileName);
            errorCode.Value = procError.ErrorCode.Code;
            errorDesc.Value = procError.ErrorCode.Description;
            procDate.Value = DateTime.Now;
            procId.Value = procError.ProcId;
            sqlProcErrors.ExecuteNonQuery();

        }

        public static void AddTransaction(Guid custid, Guid profile, string fileName, string direction, bool original, TransReference trRefs, string archive)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.CTCconnString();
            SqlCommand sqlTransaction = new SqlCommand();
            sqlTransaction.CommandType = CommandType.StoredProcedure;
            sqlTransaction.CommandText = "Add_Transaction";
            sqlTransaction.Connection = sqlConn;
            try
            {
                sqlTransaction.Parameters.AddWithValue("@T_C", custid);
                sqlTransaction.Parameters.AddWithValue("@T_P", profile);
                sqlTransaction.Parameters.AddWithValue("@T_FILENAME", Path.GetFileName(fileName));
                sqlTransaction.Parameters.AddWithValue("@T_DATETIME", DateTime.Now);
                sqlTransaction.Parameters.AddWithValue("@T_TRIAL", "N");
                sqlTransaction.Parameters.AddWithValue("@DIRECTION", direction);
                if (original)
                {
                    sqlTransaction.Parameters.AddWithValue("@ORIGINAL", 1);
                }
                else
                {
                    sqlTransaction.Parameters.AddWithValue("@ORIGINAL", 0);
                }

                sqlTransaction.Parameters.AddWithValue("T_REF1", trRefs.Reference1);
                sqlTransaction.Parameters.AddWithValue("T_REF2", trRefs.Reference2);
                sqlTransaction.Parameters.AddWithValue("T_REF3", trRefs.Reference3);
                sqlTransaction.Parameters.AddWithValue("T_REF1TYPE", trRefs.Ref1Type.ToString());
                sqlTransaction.Parameters.AddWithValue("T_REF2TYPE", trRefs.Ref2Type.ToString());
                sqlTransaction.Parameters.AddWithValue("T_REF3TYPE", trRefs.Ref3Type.ToString());
                sqlTransaction.Parameters.AddWithValue("T_ARCHIVE", Path.GetFileName(archive));
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();
                sqlConn.Open();
                sqlTransaction.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e105;
                error.Description = "SQL Connection Error:" + ex.Message;
                error.Severity = "Fatal";
                procerror.ErrorCode = error;
                procerror.FileName = fileName;
                procerror.ProcId = profile;
                AddProcessingError(procerror);
            }
            finally
            {
                sqlConn.Close();
            }


        }

        public static void AddTransactionLog(Guid p_id, String senderid, String filename, String lastresult, DateTime lastdate, String success, String cust, String recipientid)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.CTCconnString();
            Guid custid = Guid.Empty;
            try
            {
                SqlCommand addTodo = new SqlCommand("ADD_TransactionLog", sqlConn);
                addTodo.CommandType = CommandType.StoredProcedure;
                SqlParameter l_id = addTodo.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier);
                SqlParameter l_filename = addTodo.Parameters.Add("@FILENAME", SqlDbType.VarChar, 200);
                SqlParameter l_date = addTodo.Parameters.Add("@LASTDATE", SqlDbType.DateTime);
                SqlParameter l_lastResult = addTodo.Parameters.Add("@LASTRESULT", SqlDbType.VarChar, 200);
                SqlParameter l_success = addTodo.Parameters.Add("@SUCCESS", SqlDbType.Char, 1);
                SqlParameter c_id = addTodo.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();
                sqlConn.Open();
                l_id.Value = p_id;
                l_filename.Value = Path.GetFileName(filename);
                l_date.Value = lastdate;
                l_lastResult.Value = lastresult;
                custid = GetCustID(cust);
                if (custid != Guid.Empty)
                {
                    c_id.Value = custid;
                }
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                addTodo.ExecuteNonQuery();
                sqlConn.Close();
            }
            catch (SqlException exSql)
            {
                CTCErrorCode error = new CTCErrorCode();
                ProcessingErrors procerror = new ProcessingErrors();
                if (custid != Guid.Empty)
                {
                    error.Code = NodeError.e106;
                }
                else
                {
                    error.Code = NodeError.e107;
                }
                error.Description = "SQL Connection Error:" + exSql.Message;
                procerror.ErrorCode = error;
                procerror.SenderId = senderid;
                procerror.RecipientId = cust;
                procerror.FileName = filename;
                procerror.ProcId = p_id;
                AddProcessingError(procerror);
                // NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error updating Transaction Log: " + exSql.Message);

            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                if (custid != Guid.Empty)
                {
                    error.Code = NodeError.e106;
                }
                else
                {
                    error.Code = NodeError.e111;
                }
                error.Description = ex.Message;
                procerror.ErrorCode = error;
                procerror.SenderId = senderid;
                procerror.RecipientId = cust;
                procerror.ErrorCode.Description = ex.Message;
                procerror.FileName = filename;
                procerror.ProcId = p_id;
                AddProcessingError(procerror);
                // NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error updating Transaction Log(2): " + ex.Message);
                // String file = Path.GetFileName(filename);
                // File.Move(filename, Path.Combine(Globals.FailLoc, file));

            }
            finally
            {
                sqlConn.Close();
            }
        }
        public static Guid GetCustID(String senderID)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.CTCconnString();
            SqlCommand sqlCustID = new SqlCommand("SELECT @C_ID=C_ID from CUSTOMER where C_CODE = @C_CODE", sqlConn);
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlCustID.Parameters.AddWithValue("@C_CODE", senderID);
            SqlParameter CUSTID = sqlCustID.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
            CUSTID.Direction = ParameterDirection.Output;
            sqlCustID.CommandType = CommandType.Text;
            sqlCustID.ExecuteNonQuery();
            if (CUSTID.Value != DBNull.Value)
            {
                return (Guid)CUSTID.Value;
            }
            else
            {
                return Guid.Empty;
            }
        }

        public static string GetEnum(string enumType, string mapValue)
        {


            string result = string.Empty;








            try
            {
                using (SqlConnection sqlConn = new SqlConnection(Globals.connString()))
                {

                    SqlCommand sqlEnum = new SqlCommand("GetCWEnum", sqlConn);
                    sqlEnum.CommandType = CommandType.StoredProcedure;
                    SqlParameter varEnum = sqlEnum.Parameters.Add("@ENUM", SqlDbType.NChar, 50);
                    SqlParameter varEnumType = sqlEnum.Parameters.Add("@ENUMTYPE", SqlDbType.VarChar, 100);
                    SqlParameter varMapVal = sqlEnum.Parameters.Add("@MAPVALUE", SqlDbType.VarChar, 100);
                    varEnum.Direction = ParameterDirection.Output;
                    varEnumType.Value = enumType;
                    varMapVal.Value = mapValue;
                    sqlConn.Open();
                    sqlEnum.ExecuteNonQuery();
                    result = varEnum.Value.ToString().Trim();
                    sqlConn.Close();
                }





            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Accessing Database: " + ex.Message);
            }
            return result;
        }
        public static string MoveFile(string fileToMove, string folder)
        {
            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(fileToMove);
            string ext = Path.GetExtension(fileToMove);
            string newfile = Path.Combine(folder, Path.GetFileName(fileToMove));
            while (File.Exists(newfile))
            {
                newfile = Path.Combine(folder, filename + icount + ext);
                icount++;

            }

            try
            {
                File.Move(fileToMove, Path.Combine(folder, newfile));
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                newfile = "Warning: " + strEx + " exception found while moving " + filename;
            }
            return newfile;
        }

        /* public static string FtpSend(string file, CustProfileRecord cust)
        {
            string Result = string.Empty;
            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    HostName = cust.P_server.Trim(),
                    PortNumber = int.Parse(cust.P_port),
                    UserName = cust.P_username.Trim(),
                    Password = cust.P_password.Trim()
                };
                if (cust.P_ssl)
                {
                    sessionOptions.Protocol = Protocol.Sftp;
                }
                else
                {
                    sessionOptions.Protocol = Protocol.Ftp;
                }
                //try
                //{
                TransferOperationResult tr;
                using (Session session = new Session())
                {
                    session.Open(sessionOptions);
                    if (session.Opened)
                    {
                        //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + custProfile[0].P_server + ". Sending file  :" + xmlfile + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + custProfile[0].P_path + "/" + Path.GetFileName(xmlfile));
                        TransferOptions tOptions = new TransferOptions();
                        tOptions.OverwriteMode = OverwriteMode.Overwrite;
                        if (string.IsNullOrEmpty(cust.P_path))
                        {
                            cust.P_path = "";
                        }
                        else
                        {
                            cust.P_path = cust.P_path + "/";
                        }
                        tr = session.PutFiles(file, "/" + cust.P_path + Path.GetFileName(file), true, tOptions);
                        //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");
                        if (tr.IsSuccess)
                        {
                            Result = "File Sent Ok";
                            try
                            {
                                File.Delete(file);
                            }
                            catch (Exception ex)
                            {
                                Result = "File Sent Ok. Delete Error: " + ex.Message;
                            }

                        }
                        else
                        {
                            Result = "Failed: Transfer Result is invalid: ";
                        }

                    }
                    else
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                Result = "Failed: " + ex.GetType().Name + " " + ex.Message;
            }

            return Result;
        }
 */

    }


}